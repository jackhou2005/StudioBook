# This is the repository of the StudioBook by cStudio.

## What is 'StudioBook'?

StudioBook is a privacy space for people who want to hide their own things. In StudioBook, you can tell everything which hide below your heart, and needn't be worry about such problems like hacker attack, unlegal information collecting.

## Contact with Developers

If you have any good ideas, please contact with developers!

International Email Address: heisenberg99991@outlook.com

Chinese Email Address: jack_hou_2005@163.com/jack_smith_2000@163.com

## Update Records

### 2020-4-30 Update

- Fixed the bug that cannot use menubar functions after configured hot keys

- Added the footer, it shows the version of the software, and prevent some arguments on version issues.

### 2020-4-29 Update

- Continue Optimizing hot keys and changed some hot keys' combination

- Fixed some appearance issues on 'About' and other windows

### 2020-4-28 Update

- Optimized the configuration of hot keys, now you can use them freely as a bird

### 2020-4-27 Update

- Because of the epidemic situation, the update had stopped for 1 month and 12 days, sorry for your waiting. Besides, show my highest respect to those medical workers who are fighting against the plague！

- Bound all hot keys excluded these functions: open, save and save as

### 2020-3-15 Update

- Happy Pi Day! We had fixed a bug in the file reader model which was when you close the file dialogue window, the file reader window would jumped out 

### 2020-3-13 Update

- Added mode remember function, now if you opened the application, the appeared theme is as same as the theme when you last quitted

### 2020-3-9 Update

- Because of the unstable and low speed of Lantern Server, 'Connect Agency Server' function temporarily have shut down

### 2020-3-8 Update

- Mode control function advance finished, now try it!

### 2020-3-7 Update

- Changed some windows' size

- Added the menu options of changing text color

- Now advancing the mode control function

### 2020-3-4 Update

- For preventing some misunderstanding, removed some menu keys without functions

- To decreasing lines and without performance deduct, we used lambda function instead of single functions

### 2020-3-1 Update

- Fixed the bug that application stop running while using 'Connect Agency Server'

- Split the original file to increase maintainability and extendability

### 2020-2-26 Update

- Because most of people use left Control button in a very high frequency, we designed the hot key system of 'QWERASD'

### 2020-2-20 Update

- Removed 'User' and 'Terminal Mode'

- Advanced the font of document reader

- Delete some useless code

- Advanced interface design

### 2020-2-10 Update

- Added 'User' function's beta version

- Advanced Todo-List

- Removed code highlight -- actually didn't finish

- Changed some elements' name

### 2020-2-5 Update

- A test about code highlight is on going.

### 2020-2-1 Update

- Advance some code about logical judge.

### 2020-1-21 Update

- Happy Chinese new year! Everything was done in this update, go and experience.

### 2020-1-5 Update

- Happy new year! The 'Programmer Mode' is about to arrive StudioBook with its friend 'To-Do List'!

### 2019-12-26 Update

- Hey, what's up! We were just changed its name from DiaryBook to StudioBook, but changed nothing except.

### 2019-11-04 Update

- Added some files about 'DiaryBook'
