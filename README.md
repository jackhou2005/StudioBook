# StudioBook

StudioBook是一个基于Python 3.5的超轻量记事本软件，无需安装，运行代码即可开始使用。

StudioBook is a notepad software based on Python 3.5, it is a non-install software, just run the source code and start using.

点击以下链接阅读README：

Click the links below to read README:

- [中文版README](https://gitee.com/jackhou2005/StudioBook/blob/master/README%5C_zh.md)

- [README in English](https://gitee.com/jackhou2005/StudioBook/blob/master/README%5C_en.md)