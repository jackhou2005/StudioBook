from tkinter import *
from tkinter.scrolledtext import ScrolledText
import tkinter.filedialog
import webbrowser
import os

global mode_code,filename

with open('Mode.txt') as file:
    mode_code=file.read()

todo_list=[]

class File_def:
    def load(self,contents):
        try:
            global filename
            filename=tkinter.filedialog.askopenfilename(filetypes=[('All Files','*.*')],
                initialdir='//home',initialfile='Enter Filename',title='打开文件')
            with open(filename) as file:
                contents.delete('1.0', END)
                contents.insert(INSERT,file.read())
            notebook.title('StudioBook-'+filename)
                
        except FileNotFoundError:
            pass

    def save(self,contents):
        try:
            global filename
            with open(filename,'w') as file:
                file.write(contents.get('1.0',END))

        except FileNotFoundError:
            pass

    def save_as(self,contents):
        try:
            global filename
            filename=tkinter.filedialog.asksaveasfilename(filetypes=[('All Files','*.*')],
                initialdir='//home',initialfile='Enter Filename',title='另存文件')
            with open(filename,'w') as file:
                file.write(contents.get('1.0',END))
            notebook.title('StudioBook-'+filename)
                
        except FileNotFoundError:
            pass
#File_def类，用于执行文件读写类操作

class Function:
    def about(self):
        try:
            about=Toplevel(notebook)
            about.title('关于')
            about.geometry('280x220')

            global mode_code
            
            introduction=Label(about,text='StudioBook\ncStudio出品\n更新于2020.4.28\n使用软件记笔记（以每天两页A5纸为例），\n每年可以节省约730张纸，\n使用软件阅读文件能够节省的纸张更是数不胜数',
                font=('Arial',9),width=40,height=7)
            introduction.pack(side=TOP,expand=True,fill=BOTH)

            if mode_code=='1':
                introduction.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                introduction.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                introduction.config(bg='#222222',fg='#FFFFFF')

            #introduction.bind('<Control-Q>',lambda destroy:about.destroy())
            #关于界面的QWERASD快捷键配置
            #目前无法使用，先注释掉

        except:
            pass
    #关于界面函数

    def document_reader(self):
        try:
            doc=Toplevel(notebook)
            doc.title('文档阅读器-文件未载入')
            doc.geometry('700x900')

            global mode_code

            document=ScrolledText(doc,font=('Arial',9),wrap=WORD)
            document.pack(side=TOP,expand=True,fill=BOTH)

            if mode_code=='1':
                document.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                document.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                document.config(bg='#222222',fg='#FFFFFF')

            def document_opener(document):
                filename=tkinter.filedialog.askopenfilename(
                    filetypes=[('All Files','*.*')],initialdir='//home',
                    initialfile='Enter Filename',title='打开你要阅读的文件')

                doc.title('文档阅读器-'+filename)
                
                reader=' '  

                with open(filename) as file:
                    reader=file.read()
                
                document.insert(INSERT,reader)
                document.config(state=DISABLED)

            doc_menu=Menu(doc,title='文档阅读器')

            filemenu=Menu(doc_menu,tearoff=0)
            doc_menu.add_cascade(label='文件',font=('Arial',9),menu=filemenu)

            filemenu.add_command(label='打开文件',font=('Arial',9),
                command=document_opener,accelerator='CTRL+W')
            filemenu.add_command(label='退出文档阅读器',font=('Arial',9),
                command=lambda:doc.destroy(),accelerator='CTRL+SHIFT+Q')

            fontmenu=Menu(doc_menu,tearoff=0)
            doc_menu.add_cascade(label='字体设置',font=('Arial',9),
                menu=fontmenu)

            kindmenu=Menu(fontmenu,tearoff=0)
            fontmenu.add_cascade(label='字体',font=('Arial',9),menu=kindmenu)

            kindmenu.add_command(label='Arial',font=('Arial',9),
                command=lambda:document.config(font=('Arial')))
            kindmenu.add_command(label='Consolas',font=('Consolas',9),
                command=lambda:document.config(font=('Consolas')))
            kindmenu.add_command(label='Unifont',font=('Unifont',9),
                command=lambda:document.config(font=('Unifont')))

            colormenu=Menu(fontmenu,tearoff=0)
            fontmenu.add_cascade(label='颜色',font=('Arial',9),menu=colormenu)

            colormenu.add_command(label='黑色',font=('Arial',9),
                command=lambda:document.config(fg='black'))
            colormenu.add_command(label='白色',font=('Arial',9),
                command=lambda:document.config(fg='white'))
            colormenu.add_command(label='灰色',font=('Arial',9),
                command=lambda:document.config(fg='grey'))
            colormenu.add_command(label='红色',font=('Arial',9),
                command=lambda:document.config(fg='red'))
            colormenu.add_command(label='蓝色',font=('Arial',9),
                command=lambda:document.config(fg='blue'))
            colormenu.add_command(label='黄色',font=('Arial',9),
                command=lambda:document.config(fg='yellow'))

            doc.config(menu=doc_menu)

            document.bind('<Control-w>',document_opener)
            document.bind('<Control-Q>',lambda destroy:doc.destroy())
            #文档阅读器的QWERASD快捷键配置

            doc.mainloop()
        except:
            pass
    #文档阅读器函数

    def mode_control(self,input_mode_code):
        try:
            global mode_code

            mode_code=input_mode_code
            if mode_code=='1':
                contents.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                contents.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                contents.config(bg='#222222',fg='#FFFFFF')
        except:
            pass
    #模式控制函数，子函数分布在其他函数内

    def quicknote(self):
        try:
            quicknote=Toplevel(notebook)
            quicknote.title('便笺')
            quicknote.geometry('300x300')

            global mode_code

            note=ScrolledText(quicknote,font=('Arial',9))
            note.pack(side=TOP,expand=True,fill=BOTH)

            if mode_code=='1':
                note.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                note.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                note.config(bg='#222222',fg='#FFFFFF')

            #quciknote.bind('<Control-Q>',lambda destroy:quicknote.destroy())
            #便笺的QWERASD快捷键配置
            #目前无法使用，先注释掉

        except:
            pass
    #便笺函数
    
    def show_license(self):
        try:
            webbrowser.open('http://unlicense.org',new=0)
        except:
            pass
    #显示条款函数，条款为Unlicense
    
    def tool_source(self):
        try:
            tool_source=Toplevel(notebook)
            tool_source.title('工具来源')
            tool_source.geometry('200x200')

            global mode_code

            info=Label(tool_source,
                text='牛津英汉大词典\nLantern Free Agent\nOpen-VPN\nShadowscket-Qt5',
                font=('Arial',9),width=40,height=15)
            info.pack(side=TOP,expand=True,fill=BOTH)
            if mode_code=='1':
                info.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                info.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                info.config(bg='#222222',fg='#FFFFFF')
        except:
            pass
    #显示工具来源函数
#Function类，软件的主要功能


class Accelerator_def:
    def about(self,contents):
        try:
            about=Toplevel(notebook)
            about.title('关于')
            about.geometry('280x220')

            global mode_code
            
            introduction=Label(about,text='StudioBook\ncStudio出品\n更新于2020.4.28\n使用软件记笔记（以每天两页A5纸为例），\n每年可以节省约730张纸，\n使用软件阅读文件能够节省的纸张更是数不胜数',
                font=('Arial',9),width=40,height=7)
            introduction.pack(side=TOP,expand=True,fill=BOTH)

            if mode_code=='1':
                introduction.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                introduction.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                introduction.config(bg='#222222',fg='#FFFFFF')

            #introduction.bind('<Control-Q>',lambda destroy:about.destroy())
            #关于界面的QWERASD快捷键配置
            #目前无法使用，先注释掉

        except:
            pass
    #关于界面函数

    def document_reader(self,contents):
        try:
            doc=Toplevel(notebook)
            doc.title('文档阅读器-文件未载入')
            doc.geometry('700x900')

            global mode_code

            document=ScrolledText(doc,font=('Arial',9),wrap=WORD)
            document.pack(side=TOP,expand=True,fill=BOTH)

            if mode_code=='1':
                document.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                document.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                document.config(bg='#222222',fg='#FFFFFF')

            def document_opener(document):
                filename=tkinter.filedialog.askopenfilename(
                    filetypes=[('All Files','*.*')],initialdir='//home',
                    initialfile='Enter Filename',title='打开你要阅读的文件')

                doc.title('文档阅读器-'+filename)
                
                reader=' '  

                with open(filename) as file:
                    reader=file.read()
                
                document.insert(INSERT,reader)
                document.config(state=DISABLED)

            doc_menu=Menu(doc,title='文档阅读器')

            filemenu=Menu(doc_menu,tearoff=0)
            doc_menu.add_cascade(label='文件',font=('Arial',9),menu=filemenu)

            filemenu.add_command(label='打开文件',font=('Arial',9),
                command=document_opener,accelerator='CTRL+W')
            filemenu.add_command(label='退出文档阅读器',font=('Arial',9),
                command=lambda:doc.destroy(),accelerator='CTRL+SHIFT+Q')

            fontmenu=Menu(doc_menu,tearoff=0)
            doc_menu.add_cascade(label='字体设置',font=('Arial',9),
                menu=fontmenu)

            kindmenu=Menu(fontmenu,tearoff=0)
            fontmenu.add_cascade(label='字体',font=('Arial',9),menu=kindmenu)

            kindmenu.add_command(label='Arial',font=('Arial',9),
                command=lambda:document.config(font=('Arial')))
            kindmenu.add_command(label='Consolas',font=('Consolas',9),
                command=lambda:document.config(font=('Consolas')))
            kindmenu.add_command(label='Unifont',font=('Unifont',9),
                command=lambda:document.config(font=('Unifont')))

            colormenu=Menu(fontmenu,tearoff=0)
            fontmenu.add_cascade(label='颜色',font=('Arial',9),menu=colormenu)

            colormenu.add_command(label='黑色',font=('Arial',9),
                command=lambda:document.config(fg='black'))
            colormenu.add_command(label='白色',font=('Arial',9),
                command=lambda:document.config(fg='white'))
            colormenu.add_command(label='灰色',font=('Arial',9),
                command=lambda:document.config(fg='grey'))
            colormenu.add_command(label='红色',font=('Arial',9),
                command=lambda:document.config(fg='red'))
            colormenu.add_command(label='蓝色',font=('Arial',9),
                command=lambda:document.config(fg='blue'))
            colormenu.add_command(label='黄色',font=('Arial',9),
                command=lambda:document.config(fg='yellow'))

            doc.config(menu=doc_menu)

            document.bind('<Control-w>',document_opener)
            document.bind('<Control-Q>',lambda destroy:doc.destroy())
            #文档阅读器的QWERASD快捷键配置

            doc.mainloop()
        except:
            pass
    #文档阅读器函数

    def quicknote(self,contents):
        try:
            quicknote=Toplevel(notebook)
            quicknote.title('便笺')
            quicknote.geometry('300x300')

            global mode_code

            note=ScrolledText(quicknote,font=('Arial',9))
            note.pack(side=TOP,expand=True,fill=BOTH)

            if mode_code=='1':
                note.config(bg='#FFFFE0',fg='#000000')
            if mode_code=='2':
                note.config(bg='#FFFFFF',fg='#000000')
            if mode_code=='3':
                note.config(bg='#222222',fg='#FFFFFF')

            #quciknote.bind('<Control-Q>',lambda destroy:quicknote.destroy())
            #便笺的QWERASD快捷键配置
            #目前无法使用，先注释掉

        except:
            pass

    def show_license(self,contents):
        try:
            webbrowser.open('http://unlicense.org',new=0)
        except:
            pass
    #显示条款函数，条款为Unlicense
#Accelerator_def类，用于快捷键配置

class Todo_list:
    def add_todo(self):
        try:
            x=content_entry.get()

            checkbox=Checkbutton(todo_frame,text=x,font=('Arial',9))
            checkbox.pack(side=TOP,expand=True,fill=BOTH)

            todo_list.append(checkbox)
            content_entry.config(show=None)
            with open('List.txt','a') as file:
                file.write(x+'\n')
        except:
            pass
    #新建待办事项

    def reduce_todo(self):
        try:
            while True:
                checkbox=todo_list.pop()
                checkbox.destroy()
        except:
            pass
    #删除所有待办事项
#Todo_list类，用于待办事项功能

notebook=Tk()
notebook.title('StudioBook')
notebook.geometry('900x800')

File=File_def()
Func=Function()
Accelerator=Accelerator_def()
Todo=Todo_list()

footer=Label(notebook,text='快捷键配置：QWERASD    作者：JackHou2005    版本：v2.2 Developed',
    font=('Arial',9),width=200,height=1)
footer.pack(side=BOTTOM,fill=Y)

todo_frame=Frame(notebook)
todo_frame.pack(side=RIGHT,fill=Y)

btn_checklist=Button(todo_frame,text='创建待办事项',font=('Arial',9),
    command=Todo.add_todo)
btn_checklist.pack(side=TOP)

content_entry=Entry(todo_frame,show=None,font=('Arial',9))
content_entry.pack(side=TOP)

btn_cl_destroy=Button(todo_frame,text='清除所有待办事项',font=('Arial',9),
    command=Todo.reduce_todo)
btn_cl_destroy.pack(side=BOTTOM)

contents=ScrolledText(notebook,font=('Arial',9),wrap=WORD)
contents.pack(side=BOTTOM,expand=True,fill=BOTH) 

if mode_code=='1':
    contents.config(bg='#FFFFE0',fg='#000000')
if mode_code=='2':
    contents.config(bg='#FFFFFF',fg='#000000')
if mode_code=='3':
    contents.config(bg='#222222',fg='#FFFFFF')

menubar=Menu(notebook,title='StudioBook')

filemenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='文件',font=('Arial',9),menu=filemenu)

filemenu.add_command(label='打开文件',font=('Arial',9),
    command=lambda:File.load(contents),accelerator='CTRL+W')
filemenu.add_command(label='保存文件',font=('Arial',9),
    command=lambda:File.save(contents),accelerator='CTRL+S')
filemenu.add_command(label='另存文件',font=('Arial',9),
    command=lambda:File.save_as(contents),accelerator='CTRL+SHIFT+S')
filemenu.add_command(label='文档阅读器',font=('Arial',9),
    command=Func.document_reader,accelerator='CTRL+R')
filemenu.add_separator()
filemenu.add_command(label='退出StudioBook',font=('Arial',9),
    command=quit,accelerator='CTRL+SHIFT+Q')

toolmenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='工具',font=('Arial',9),menu=toolmenu)

toolmenu.add_command(label='词典',font=('Arial',9),command=DISABLED,
    accelerator='CTRL+D')
#词典函数，一周内解决
toolmenu.add_command(label='便笺',font=('Arial',9),command=Func.quicknote,
    accelerator='CTRL+Q')
toolmenu.add_command(label='发送邮件',font=('Arial',9),command=DISABLED,
    accelerator='CTRL+E')
#发送邮件函数，一月内解决

fontmenu=Menu(toolmenu,tearoff=0)
toolmenu.add_cascade(label='字体设置',font=('Arial',9),menu=fontmenu)

kindmenu=Menu(fontmenu,tearoff=0)
fontmenu.add_cascade(label='字体',font=('Arial',9),menu=kindmenu)

kindmenu.add_command(label='Arial',font=('Arial',9),
    command=lambda:contents.config(font=('Arial')))
kindmenu.add_command(label='Consolas',font=('Consolas',9),
    command=lambda:contents.config(font=('Consolas')))
kindmenu.add_command(label='Unifont',font=('Unifont',9),
    command=lambda:contents.config(font=('Unifont')))

colormenu=Menu(fontmenu,tearoff=0)
fontmenu.add_cascade(label='颜色',font=('Arial',9),menu=colormenu)

colormenu.add_command(label='黑色',font=('Arial',9),
    command=lambda:contents.config(fg='black'))
colormenu.add_command(label='白色',font=('Arial',9),
    command=lambda:contents.config(fg='white'))
colormenu.add_command(label='灰色',font=('Arial',9),
    command=lambda:contents.config(fg='grey'))
colormenu.add_command(label='红色',font=('Arial',9),
    command=lambda:contents.config(fg='red'))
colormenu.add_command(label='蓝色',font=('Arial',9),
    command=lambda:contents.config(fg='blue'))
colormenu.add_command(label='黄色',font=('Arial',9),
    command=lambda:contents.config(fg='yellow'))

modemenu=Menu(toolmenu,tearoff=0)
toolmenu.add_cascade(label='模式设置',font=('Arial',9),menu=modemenu)

modemenu.add_command(label='笔记本模式',font=('Arial',9),
    command=lambda:Func.mode_control('1'))
modemenu.add_command(label='明亮模式',font=('Arial',9),
    command=lambda:Func.mode_control('2'))
modemenu.add_command(label='黑暗模式',font=('Arial',9),
    command=lambda:Func.mode_control('3'))

aboutmenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='关于',font=('Arial',9),menu=aboutmenu)

aboutmenu.add_command(label='关于',font=('Arial',9),
    command=Func.about,accelerator='CTRL+SHIFT+A')
aboutmenu.add_command(label='条款',font=('Arial',9),
    command=Func.show_license,accelerator='CTRL+L')
aboutmenu.add_command(label='工具来源',font=('Arial',9),
    command=Func.tool_source)

notebook.config(menu=menubar)

contents.bind('<Control-w>',lambda load:File.load(contents))
contents.bind('<Control-s>',lambda save:File.save(contents))
contents.bind('<Control-S>',lambda save_as:File.save_as(contents))
contents.bind('<Control-r>',Accelerator.document_reader)
contents.bind('<Control-A>',Accelerator.about)
contents.bind('<Control-q>',Accelerator.quicknote)
contents.bind('<Control-l>',Accelerator.show_license)
contents.bind('<Control-Q>',quit)
#主窗体的QWERASD快捷键配置

notebook.mainloop()

with open('List.txt','w') as file:
    file.write(' ')
with open('Mode.txt','w') as file:
    file.write(mode_code)
