#该文件已归档，若无仓库主授权，请勿修改

"""
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
-- Zen of Python by Tim Peters
"""

from tkinter import *
import webbrowser
from tkinter.scrolledtext import ScrolledText
import tkinter.filedialog

global mode
mode=False
#mode用于模式控制

todo_list=[]
usr_name_list=[]
usr_pswd_list=[]

def info_get(name):
    return name.get()

class File_def:
    def load(self,contents):
        try:
            if mode is True:
                with open('Code.txt') as file:
                    contents.delete('1.0', END)
                    contents.insert(INSERT,file.read())
            else:
                with open('Note.txt') as file:
                    contents.delete('1.0', END)
                    contents.insert(INSERT,file.read())
        except FileNotFoundError:
            pass

    def save(self,contents):
        try:
            if mode is True:
                with open('Code.txt','w') as file:
                    file.write(contents.get('1.0',END))
            else:
                with open('Note.txt','w') as file:
                    file.write(contents.get('1.0',END))
        except FileNotFoundError:
            pass
#File_def类，用于执行文件读写类操作


class Function:
    def about(self):
        global mode
        
        try:
            about=Toplevel(notebook)
            about.title('关于')
            about.geometry('300x250')

            no_link_introduction=Label(about,text='StudioBook\ncStudio出品\n更新于2020.2.14\n\n',bg='#FFFFE0',font=('Arial',9),width=30,height=7)
            no_link_introduction.pack(side=TOP)

            linked_introduction=Label(about,text='制作不易，若是认可我们的项目，\n请为我们捐款',bg='#FFFFE0',font=('Arial',9),fg='#3399FF',width=30,height=5)
            linked_introduction.pack(side=TOP)

            def open_url(event):
                webbrowser.open('https://gitee.com/jackhou2005/FeatherNote/blob/master/StudioBook/QR%20Code.png',new=0)

            linked_introduction.bind('<Button-1>',open_url)
            
            if mode is True:
                no_link_introduction.config(font=('Uniform',9),bg='#000000',fg='#FFFFFF')
                linked_introduction.config(font=('Uniform',9),bg='#000000',fg='#3399FF')
            else:
                no_link_introduction.config(font=('Arial',9),bg='#FFFFE0',fg='#000000')
                linked_introduction.config(font=('Arial',9),bg='#FFFFE0',fg='#3399FF')
        except:
            pass
    #关于界面函数

    def document(self):
        global mode
        
        try:
            doc=Toplevel(notebook)
            doc.title('cStudio文档阅读器')
            doc.geometry('500x700')

            reader=' '

            document=ScrolledText(doc,font=('Arial',9),bg='#FFFFE0',fg='#000000')
            document.pack(side=TOP,expand=True,fill=BOTH)
            
            filename=tkinter.filedialog.askopenfilename(filetypes=[('All Files','*.*')],initialdir='//home',initialfile='Enter Filename',title='打开你要阅读的文件')
                
            with open(filename) as file:
                reader=file.read()
                
            document.insert(INSERT,reader)
            document.config(state=DISABLED)
                
            if mode is True:
                document.config(font=('Unifont',9),bg='#000000',fg='#FFFFFF')
            else:
                document.config(font=('Arial',9),bg='#FFFFE0',fg='#000000')

            doc.mainloop()
        except:
            pass
    #文档阅读器函数
        
    def terminal_mode(self):
        global mode
        
        if mode is False:
            contents.config(font=('Unifont',9),bg='#000000',fg='#FFFFFF')
            notebook.title('代码板')
            btn_load.config(text='打开代码')
            btn_save.config(text='保存代码')
            btn_document.config(text='代码阅读器')
            btn_about.config(text='关于')
            btn_mode.config(text='笔记本模式')
            mode=True
        else:
            contents.config(font=('Arial',9),bg='#FFFFE0',fg='#000000')
            notebook.title('笔记本')
            btn_load.config(text='打开笔记本')
            btn_save.config(text='保存内容')
            btn_document.config(text='文档阅读器')
            btn_about.config(text='关于')
            btn_mode.config(text='终端模式')
            mode=False
    #模式控制主函数，子函数分散在各个函数中
#Function类，软件的主要功能

class Todo_list:
    def add_todo(self):
        try:
            x=info_get(content_entry)
            checkbox=Checkbutton(tod_frame,text=x,font=('Arial',9))
            checkbox.pack(side=TOP)
            todo_list.append(checkbox)
            content_entry.config(show=None)
            with open('List.txt','a') as file:
                file.write(x+'\n')
        except:
            pass
    #新建待办事项

    def reduce_todo(self):
        try:
            while True:
                checkbox=todo_list.pop()
                checkbox.destroy()
        except:
            pass
    #删除所有待办事项
#Todo_list类，用于待办事项功能
        
notebook=Tk()
notebook.title('笔记本')
notebook.geometry('900x800')

File=File_def()
Func=Function()
Todo=Todo_list()


todo_frame=Frame(notebook)
todo_frame.pack(side=RIGHT,expand=True,fill=Y)

content_entry=Entry(todo_frame,show=None,font=('Arial',9))
content_entry.pack(side=TOP)

btn_checklist=Button(todo_frame,text='创建待办事项',font=('Arial',9),command=Todo.add_todo)
btn_checklist.pack(side=TOP)

btn_cl_destroy=Button(todo_frame,text='清除所有待办事项',font=('Arial',9),command=Todo.reduce_todo)
btn_cl_destroy.pack(side=BOTTOM)

contents=ScrolledText(notebook,bg='#FFFEE0',font=('Arial',9))
contents.pack(side=BOTTOM,expand=True,fill=BOTH) 

btn_load=Button(notebook,text='打开笔记本',font=('Arial',9),command=lambda:File.load(contents))
btn_load.pack(side=LEFT)

btn_save=Button(notebook,text='保存内容',font=('Arial',9),command=lambda:File.save(contents))
btn_save.pack(side=LEFT)

btn_document=Button(notebook,text='文档阅读器',font=('Arial',9),command=Func.document)
btn_document.pack(side=LEFT)

btn_about=Button(notebook,text='关于',font=('Arial',9),command=Func.about)
btn_about.pack(side=RIGHT)

btn_mode=Button(notebook,text='终端模式',font=('Arial',9),command=Func.terminal_mode)
btn_mode.pack(side=RIGHT)

notebook.mainloop()

with open('List.txt','w') as file:
    file.write(' ')