#该文件已归档，若无仓库主授权，请勿修改

from tkinter import *
from tkinter.scrolledtext import ScrolledText
from file_def import File_def
import tkinter.filedialog
import webbrowser
import os

global number
number=1

todo_list=[]

class Todo_list:
    def add_todo(self):
        try:
            x=content_entry.get()

            checkbox=Checkbutton(todo_frame,text=x,font=('Arial',12))
            checkbox.pack(side=TOP)

            todo_list.append(checkbox)
            content_entry.config(show=None)
            with open('List.txt','a') as file:
                file.write(x+'\n')
        except:
            pass
    #新建待办事项

    def reduce_todo(self):
        try:
            while True:
                checkbox=todo_list.pop()
                checkbox.destroy()
        except:
            pass
    #删除所有待办事项
#Todo_list类，用于待办事项功能

class Function:
    def about(self):
        try:
            about=Tk()
            about.title('关于')
            about.geometry('280x220')

            global number
            
            if number==1:
                no_link_introduction_1=Label(about,text='StudioBook\ncStudio出品\n更新于2020.3.7',bg='#FFFFE0',font=('Arial',12),fg='#000000',width=40,height=4)
                no_link_introduction_1.pack(side=TOP)
            if number==2:
                no_link_introduction_1=Label(about,text='StudioBook\ncStudio出品\n更新于2020.3.7',bg='#FFFFFF',font=('Arial',12),fg='#000000',width=40,height=4)
                no_link_introduction_1.pack(side=TOP)
            if number==3:
                no_link_introduction_1=Label(about,text='StudioBook\ncStudio出品\n更新于2020.3.7',bg='#222222',font=('Arial',12),fg='#FFFFFF',width=40,height=4)
                no_link_introduction_1.pack(side=TOP)

            linked_introduction=Label(about,text='开发不易，若是认可我们的项目，\n请为我们捐款\n',bg='#222222',font=('Arial',12),fg='#3312FF',width=40,height=3)
            linked_introduction.pack(side=TOP)

            def open_url_1(event):
                webbrowser.open('https://gitee.com/jackhou2005/StudioBook/blob/master/QR%20Code.png',new=2)

            linked_introduction.bind('<Button-1>',open_url_1)

            if number==1:
                no_link_introduction_2=Label(about,text='使用软件记笔记（以每天两页A5纸为例），\n每年可以节省约730张纸，\n使用软件阅读文件能够节省的纸张更是数不胜数',bg='#FFFEE0',font=('Arial',12),fg='#000000',width=40,height=4)
                no_link_introduction_2.pack(side=TOP)

                no_link_introduction_3=Label(about,text='为中国绿化基金会捐款：\nhttp://www.cgf.org.cn/juanzeng/',bg='#FFFFE0',font=('Arial',12),fg='#000000',width=40,height=3)
                no_link_introduction_3.pack(side=TOP)
            if number==2:
                no_link_introduction_2=Label(about,text='使用软件记笔记（以每天两页A5纸为例），\n每年可以节省约730张纸，\n使用软件阅读文件能够节省的纸张更是数不胜数',bg='#FFFFFF',font=('Arial',12),fg='#000000',width=40,height=4)
                no_link_introduction_2.pack(side=TOP)

                no_link_introduction_3=Label(about,text='为中国绿化基金会捐款：\nhttp://www.cgf.org.cn/juanzeng/',bg='#FFFFFF',font=('Arial',12),fg='#000000',width=40,height=3)
                no_link_introduction_3.pack(side=TOP)
            if number==3:
                no_link_introduction_2=Label(about,text='使用软件记笔记（以每天两页A5纸为例），\n每年可以节省约730张纸，\n使用软件阅读文件能够节省的纸张更是数不胜数',bg='#222222',font=('Arial',12),fg='#FFFFFF',width=40,height=4)
                no_link_introduction_2.pack(side=TOP)

                no_link_introduction_3=Label(about,text='为中国绿化基金会捐款：\nhttp://www.cgf.org.cn/juanzeng/',bg='#222222',font=('Arial',12),fg='#FFFFFF',width=40,height=3)
                no_link_introduction_3.pack(side=TOP)
        except:
            pass
    #关于界面函数
    
    def lantern_agency(self):
        def connect_agency():
            os.system('gnome-terminal -e lantern')
            
        agency=Tk()
        agency.title('连接代理服务器')
        agency.geometry('200x20')
        
        connect_command=Button(agency,text='连接',font=('Arial',12),command=connect_agency)
        connect_command.pack(side=TOP)
    #连接代理服务器函数

    def document_reader(self):
        try:
            doc=Tk()
            doc.title('文档阅读器')
            doc.geometry('700x900')

            global number

            if number==1:
                document=ScrolledText(doc,bg='#FFFFE0',font=('Arial',12),fg='#000000')
                document.pack(side=TOP,expand=True,fill=BOTH)
            if number==2:
                document=ScrolledText(doc,bg='#FFFFFF',font=('Arial',12),fg='#000000')
                document.pack(side=TOP,expand=True,fill=BOTH)
            if number==3:
                document=ScrolledText(doc,bg='#222222',font=('Arial',12),fg='#FFFFFF')
                document.pack(side=TOP,expand=True,fill=BOTH)

            filename=tkinter.filedialog.askopenfilename(filetypes=[('All Files','*.*')],initialdir='//home',initialfile='Enter Filename',title='打开你要阅读的文件')
                        
            reader=' '    

            with open(filename) as file:
                reader=file.read()
            
            document.insert(INSERT,reader)
            document.config(state=DISABLED)
        except:
            pass
    #文档阅读器函数

    def mode_control(self,input_number):
        global number
        number=input_number
        if number==1:
            contents.config(bg='#FFFFE0')
        if number==2:
            contents.config(bg='#FFFFFF')
        if number==3:
            contents.config(bg='#222222')
    
    def quicknote(self):
        try:
            quicknote=Tk()
            quicknote.title('便笺')
            quicknote.geometry('300x300')

            if number==1:
                note=ScrolledText(quicknote,bg='#FFFFE0',font=('Arial',12),fg='#000000')
                note.pack(side=TOP,expand=True,fill=BOTH)
            if number==2:
                note=ScrolledText(quicknote,bg='#FFFFFF',font=('Arial',12),fg='#000000')
                note.pack(side=TOP,expand=True,fill=BOTH)
            if number==3:
                note=ScrolledText(quicknote,bg='#222222',font=('Arial',12),fg='#FFFFFF')
                note.pack(side=TOP,expand=True,fill=BOTH)
        except:
            pass
    #便笺函数
    
    def show_license(self):
        try:
            webbrowser.open('https://gitee.com/jackhou2005/StudioBook/blob/master/LICENSE',new=1)
        except:
            pass
    #显示条款函数，条款为LGPL-3.0
    
    def tool_source(self):
        try:
            tool_source=Tk()
            tool_source.title('工具来源')
            tool_source.geometry('200x200')

            if number==1:
                info=Label(tool_source,text='牛津英汉大词典\nLantern Free Agent\nOpen-VPN\nShadowscket-Qt5',bg='#FFFFE0',font=('Arial',12),fg='#000000',width=40,height=15)
                info.pack(side=TOP)
            if number==2:
                info=Label(tool_source,text='牛津英汉大词典\nLantern Free Agent\nOpen-VPN\nShadowscket-Qt5',bg='#FFFFFF',font=('Arial',12),fg='#000000',width=40,height=15)
                info.pack(side=TOP)
            if number==3:
                info=Label(tool_source,text='牛津英汉大词典\nLantern Free Agent\nOpen-VPN\nShadowscket-Qt5',bg='#222222',font=('Arial',12),fg='#FFFFFF',width=40,height=15)
                info.pack(side=TOP)
            
        except:
            pass
    #显示工具来源函数
#Function类，软件的主要功能


notebook=Tk()
notebook.title('笔记本')
notebook.geometry('900x800')

File=File_def()
Func=Function()
Todo=Todo_list()

todo_frame=Frame(notebook)
todo_frame.pack(side=RIGHT)

btn_checklist=Button(todo_frame,text='创建待办事项',font=('Arial',12),command=Todo.add_todo)
btn_checklist.pack(side=TOP)

content_entry=Entry(todo_frame,show=None,font=('Arial',12))
content_entry.pack(side=TOP)

btn_cl_destroy=Button(todo_frame,text='清除所有待办事项',font=('Arial',12),command=Todo.reduce_todo)
btn_cl_destroy.pack(side=BOTTOM)

contents=ScrolledText(notebook,bg='#FFFEE0',font=('Arial',12),fg='#000000')
contents.pack(side=BOTTOM,expand=True,fill=BOTH) 

menubar=Menu(notebook,title='StudioBook')

filemenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='文件',font=('Arial',12),menu=filemenu)

filemenu.add_command(label='打开文件',font=('Arial',12),command=lambda:File.load(contents))
filemenu.add_command(label='保存文件',font=('Arial',12),command=lambda:File.save(contents))
filemenu.add_command(label='文档阅读器',font=('Arial',12),command=Func.document_reader)
filemenu.add_separator()
filemenu.add_command(label='退出StudioBook',font=('Arial',12),command=notebook.quit)

toolmenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='工具',font=('Arial',12),menu=toolmenu)

toolmenu.add_command(label='便笺',font=('Arial',12),command=Func.quicknote)
toolmenu.add_command(label='连接代理服务器',font=('Arial',12),command=Func.lantern_agency)

fontmenu=Menu(toolmenu,tearoff=0)
toolmenu.add_cascade(label='字体设置',font=('Arial',12),menu=fontmenu)

kindmenu=Menu(fontmenu,tearoff=0)
fontmenu.add_cascade(label='字体',font=('Arial',12),menu=kindmenu)

kindmenu.add_command(label='Arial',font=('Arial',12),command=lambda:contents.config(font=('Arial')))
kindmenu.add_command(label='Consolas',font=('Consolas',12),command=lambda:contents.config(font=('Consolas')))
kindmenu.add_command(label='Unifont',font=('Unifont',12),command=lambda:contents.config(font=('Unifont')))

colormenu=Menu(fontmenu,tearoff=0)
fontmenu.add_cascade(label='颜色',font=('Arial',12),menu=colormenu)

colormenu.add_command(label='黑色',font=('Arial',12),command=lambda:contents.config(fg='black'))
colormenu.add_command(label='白色',font=('Arial',12),command=lambda:contents.config(fg='white'))
colormenu.add_command(label='灰色',font=('Arial',12),command=lambda:contents.config(fg='grey'))
colormenu.add_command(label='红色',font=('Arial',12),command=lambda:contents.config(fg='red'))
colormenu.add_command(label='蓝色',font=('Arial',12),command=lambda:contents.config(fg='blue'))
colormenu.add_command(label='黄色',font=('Arial',12),command=lambda:contents.config(fg='yellow'))

modemenu=Menu(toolmenu,tearoff=0)
toolmenu.add_cascade(label='模式设置',font=('Arial',12),menu=modemenu)

modemenu.add_command(label='笔记本模式',font=('Arial',12),command=lambda:Func.mode_control(1))
modemenu.add_command(label='明亮模式',font=('Arial',12),command=lambda:Func.mode_control(2))
modemenu.add_command(label='黑暗模式',font=('Arial',12),command=lambda:Func.mode_control(3))

aboutmenu=Menu(menubar,tearoff=0)
menubar.add_cascade(label='关于',font=('Arial',12),menu=aboutmenu)

aboutmenu.add_command(label='关于',font=('Arial',12),command=Func.about)
aboutmenu.add_command(label='条款',font=('Arial',12),command=Func.show_license)
aboutmenu.add_command(label='工具来源',font=('Arial',12),command=Func.tool_source)

notebook.config(menu=menubar)

notebook.mainloop()

with open('List.txt','w') as file:
    file.write(' ')
